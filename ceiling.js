import * as THREE from 'three';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';

export const createCeiling = (scene, textureLoader) => {
  const ceilingTexture = textureLoader.load('img/white-texture.jpg');
  const ceilingGeometry = new THREE.PlaneGeometry(45, 40);
  const ceilingMaterial = new THREE.MeshLambertMaterial({
    map: ceilingTexture,
  });
  const ceilingPlane = new THREE.Mesh(ceilingGeometry, ceilingMaterial);

  ceilingPlane.rotation.x = Math.PI / 2;
  ceilingPlane.position.y = 10;

  scene.add(ceilingPlane);

  // Add a chandelier
  const chandelierLoader = new GLTFLoader();
  chandelierLoader.load('models/chandelier.glb', (gltf) => {
    const chandelier = gltf.scene;
    
    // Scale the chandelier to make it bigger
    chandelier.scale.set(100, 100, 100);  // increase these values to make the chandelier bigger

    // Position the chandelier in the center of the ceiling
    chandelier.position.set(0, 10, 0); 

    scene.add(chandelier);
  });

  
};