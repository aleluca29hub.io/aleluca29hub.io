import { defineConfig } from 'vite'

export default defineConfig({
    build: {
        minify: false,
    },
    base: '/aleluca29.github.io/'
})