import * as THREE from 'three';
import { paintingData } from './paintingData.js';

export function createPaintings(scene, textureLoader) {
  let paintings = [];

  const frameTexture = new THREE.TextureLoader().load(
    'img/frame2.png', // Make sure the path and extension are correct
    () => {},
    (xhr) => {
      console.log((xhr.loaded / xhr.total * 100) + '% loaded');
    },
    (error) => {
      console.log('An error happened', error);
    }
  );

  paintingData.forEach((data) => {
    const painting = new THREE.Mesh(
      new THREE.PlaneGeometry(data.width, data.height),
      new THREE.MeshLambertMaterial({ map: textureLoader.load(data.imgSrc) })
    );

    painting.position.set(data.position.x, data.position.y, data.position.z);
    painting.rotation.y = data.rotationY;

    painting.userData = {
      type: 'painting',
      info: data.info,
    };

    painting.castShadow = true;
    painting.receiveShadow = true;

    const frameThickness = 0.2;
    const frameMaterial = new THREE.MeshBasicMaterial({ map: frameTexture });

    // Create a frame for each side of the painting
    ['left', 'right', 'top', 'bottom'].forEach((side) => {
      let frameGeometry;
      switch (side) {
        case 'left':
        case 'right':
          frameGeometry = new THREE.BoxGeometry(frameThickness, data.height + 2 * frameThickness, frameThickness);
          break;
        case 'top':
        case 'bottom':
          frameGeometry = new THREE.BoxGeometry(data.width + 2 * frameThickness, frameThickness, frameThickness);
          break;
      }
      const frame = new THREE.Mesh(frameGeometry, frameMaterial);
      switch (side) {
        case 'left':
          frame.position.set(-data.width / 2 - frameThickness / 2, 0, -frameThickness / 2);
          break;
        case 'right':
          frame.position.set(data.width / 2 + frameThickness / 2, 0, -frameThickness / 2);
          break;
        case 'top':
          frame.position.set(0, data.height / 2 + frameThickness / 2, -frameThickness / 2);
          break;
        case 'bottom':
          frame.position.set(0, -data.height / 2 - frameThickness / 2, -frameThickness / 2);
          break;
      }

      frame.castShadow = true;
      frame.receiveShadow = true;

      painting.add(frame);
    });

    paintings.push(painting);
  });

  paintings.forEach(painting => scene.add(painting));

  return paintings;
}
