import * as THREE from 'three';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';

export const setupFloor = (scene) => {
  const textureLoader = new THREE.TextureLoader();
  const floorTexture = textureLoader.load('img/wood.png');

  floorTexture.wrapS = THREE.RepeatWrapping;
  floorTexture.wrapT = THREE.RepeatWrapping;
  floorTexture.repeat.set(20, 20);

  const planeGeometry = new THREE.PlaneGeometry(45, 45);
  const planeMaterial = new THREE.MeshPhongMaterial({
    map: floorTexture,
    side: THREE.DoubleSide,
  });

  const floorPlane = new THREE.Mesh(planeGeometry, planeMaterial);

  floorPlane.rotation.x = Math.PI / 2;
  floorPlane.position.y = -Math.PI;

  scene.add(floorPlane);

  // Load the 3D bean bag model
  const gltfLoader = new GLTFLoader();
  gltfLoader.load(
    'models/bean_bag.glb',
    (gltf) => {
      const beanBagModel = gltf.scene;
      beanBagModel.scale.set(5, 5, 5); // Adjust the scale of the bean bag
      beanBagModel.position.set(8, -Math.PI, 0); // Position the bean bag on the floor

      scene.add(beanBagModel);

      // Load the 3D plant model
      gltfLoader.load(
        'monstera_deliciosa_potted_mid-century_plant.glb',
        (gltf) => {
          const plantModel = gltf.scene;
          plantModel.scale.set(5, 5, 5); // Make the plant very large

          // Position the plant behind the bean bag
          const plantOffset = 5; // Adjust the offset distance
          plantModel.position.set(
            beanBagModel.position.x + beanBagModel.scale.x / 2 + plantOffset,
            -Math.PI + plantModel.scale.y / 2,
            beanBagModel.position.z
          );

          scene.add(plantModel);

        },
        undefined, // onProgress callback not needed
        (error) => console.error(error) // Add error handling
      );
      

    },
    undefined, // onProgress callback not needed
    (error) => console.error(error) // Add error handling
  );
} 
